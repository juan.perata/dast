# GitLab DAST

[![pipeline status](https://gitlab.com/gitlab-org/security-products/dast/badges/master/pipeline.svg)](https://gitlab.com/gitlab-org/security-products/dast/commits/master)
[![coverage report](https://gitlab.com/gitlab-org/security-products/dast/badges/master/coverage.svg)](https://gitlab.com/gitlab-org/security-products/dast/commits/master)

GitLab tool for running Dynamic Application Security Testing (DAST) on provided
web site.

## How to use

1. Make your application or web site reachable on some URL. Exemple: http://mysite.localhost
1. `cd` into an empty directory, it will receive the JSON report file.
1. Run the Docker image:

    ```sh
    docker run \
      --interactive --tty --rm \
      --volume "$PWD":/output \
      registry.gitlab.com/gitlab-org/security-products/dast:${VERSION:-latest} /analyze -t http://mysite.localhost
    ```

    `VERSION` can be replaced with the latest available release matching your GitLab version. See [Versioning](#versioning-and-release-process) for more details.

1. The results will be displayed and also stored in `gl-dast-report.json`

    To run an authenticated scan, run the Docker image with parameters:

    ```sh
    docker run --rm -i -v $(pwd)/wrk:/zap/wrk/:rw dast /analyze \
      -t http://mysite.localhost/users/sign_in \
      --auth-url http://mysite.localhost/users/sign_in \
      --auth-username someone \
      --auth-password p@ssw0rd \
      --auth-username-field "user[login]" \
      --auth-password-field "user[password]" \
      --auth-exclude-urls http://mysite.localhost/logout_1,http://mysite.localhost/logout_2 # optional, URLs to skip during the authenticated scan; comma-separated, no spaces in between
    ```
    
    **Caution:** `-t` option with the website address must always come before `--auth-*` options
    
## Settings

### Command line arguments

DAST is a wrapper around the ZAP scanning scripts ([baseline](https://github.com/zaproxy/zaproxy/wiki/ZAP-Baseline-Scan)
or [full](https://github.com/zaproxy/zaproxy/wiki/ZAP-Full-Scan) scan). It accepts all arguments those scripts recognize
(the arguments are the same). The choice of the scan type depends on the `DAST_FULL_SCAN_ENABLED` environment variable
value, see below.

### Environment variables

| Environment variable             | Default | Function |
|----------------------------------|---------|----------|
| DAST_TARGET_AVAILABILITY_TIMEOUT | 60      | Time limit in seconds to wait for target availability. Scan is attempted nevertheless if it runs out. Integer. |
| DAST_FULL_SCAN_ENABLED           | false   | Switches the tool to execute [ZAP Full Scan](https://github.com/zaproxy/zaproxy/wiki/ZAP-Full-Scan) instead of [ZAP Baseline Scan](https://github.com/zaproxy/zaproxy/wiki/ZAP-Baseline-Scan). Boolean. `true`, `True`, or `1` are considered as true value, otherwise false. |

## Development

### Build image

```sh
docker build -t dast .
```

### Run locally

To run DAST locally and perform a scan on a docker application listening on port 80 named `myapp`:

```sh
docker run -rm --name myapp -d myapp /some/adequate/startup/command
docker run -ti --rm --link myapp:myapp -v "$PWD":/output dast /analyze -t http://myapp
```

### Tests

To run the tests:

```sh
./test.sh
```

## Versioning and release process

Please check the [Release Process documentation](https://gitlab.com/gitlab-org/security-products/release/blob/master/docs/release_process.md).

# Contributing

If you want to help and extend the list of supported scanners, read the
[contribution guidelines](CONTRIBUTING.md).

