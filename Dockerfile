FROM owasp/zap2docker-weekly

USER root

WORKDIR /output

# Install a Firefox version that is compatible with Selenium
RUN apt-get -y remove firefox && \
      cd /opt && \
      wget http://ftp.mozilla.org/pub/firefox/releases/59.0.2/linux-x86_64/en-US/firefox-59.0.2.tar.bz2 && \
      tar -xvjf firefox-59.0.2.tar.bz2 && \
      rm firefox-59.0.2.tar.bz2 && \
      ln -s /opt/firefox/firefox /usr/bin/firefox

# Install Selenium and Mozilla webdriver
RUN cd /opt && \
      wget https://github.com/mozilla/geckodriver/releases/download/v0.19.1/geckodriver-v0.19.1-linux64.tar.gz && \
      tar -xvzf geckodriver-v0.19.1-linux64.tar.gz && \
      rm geckodriver-v0.19.1-linux64.tar.gz && \
      chmod +x geckodriver && \
      ln -s /opt/geckodriver /usr/bin/geckodriver && \
      export PATH=$PATH:/usr/bin/geckodriver

# Add custom ZAP hook that handles authentication
COPY src/hooks.py /home/zap/.zap_hooks.py

# Wrap zap-baseline.py and zap-full-scan.py to support cli parameters for authentication
RUN /bin/bash -c \
  'mv /zap/zap-baseline.py /zap/zap_baseline_original.py && mv /zap/zap-full-scan.py /zap/zap_full_scan_original.py'

COPY --chown=zap src/* /zap/

ADD analyze /analyze

RUN apt-get update && apt-get install -y bash && rm -rf /var/lib/apt/lists/*

USER zap

RUN pip install --user selenium==3.10.0 pyvirtualdisplay==0.2.1

ENTRYPOINT []
CMD ["/analyze"]
