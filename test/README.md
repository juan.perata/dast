# Test Documentation

## Basic Tests

To execute DAST against a [simple web site](./web/index.html), execute from the project's root dir

```sh
./test/test-basic.sh
```

## WebGoat Tests

To execute DAST against WebGoat 8, execute from the project's root dir

```sh
./test/test-webgoat.sh
```

WebGoat is started with a [prepopulated database](./webgoat-data.tar.gz) in which a user was already created. The username is `someone` and password is `P@ssw0rd`. DAST will use these credentials to perfrom an authenticated scan of WebGoat.

These were the steps to create the prepopulated database:

1. Start the WebGoat docker image.
1. Register a new user in WebGoat. WebGoat will persist this user in its database located at `/home/webgoat/.webgoat-8.0.0.M21/`.
1. Copy the files in `/home/webgoat/.webgoat-8.0.0.M21/`.
1. When starting a new WebGoat instance, restore the files copied in the previous step to the same directory.
