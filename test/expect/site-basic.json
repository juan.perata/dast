[
  {
    "@port": "80",
    "@host": "nginx",
    "@name": "http://nginx",
    "alerts": [
      {
        "count": "5",
        "riskdesc": "Low (Medium)",
        "name": "Web Browser XSS Protection Not Enabled",
        "reference": "<p>https://www.owasp.org/index.php/XSS_(Cross_Site_Scripting)_Prevention_Cheat_Sheet</p><p>https://www.veracode.com/blog/2014/03/guidelines-for-setting-security-headers/</p>",
        "otherinfo": "<p>The X-XSS-Protection HTTP response header allows the web server to enable or disable the web browser's XSS protection mechanism. The following values would attempt to enable it: </p><p>X-XSS-Protection: 1; mode=block</p><p>X-XSS-Protection: 1; report=http://www.example.com/xss</p><p>The following values would disable it:</p><p>X-XSS-Protection: 0</p><p>The X-XSS-Protection HTTP response header is currently supported on Internet Explorer, Chrome and Safari (WebKit).</p><p>Note that this alert is only raised if the response body could potentially contain an XSS payload (with a text-based content type, with a non-zero length).</p>",
        "sourceid": "3",
        "confidence": "2",
        "alert": "Web Browser XSS Protection Not Enabled",
        "instances": [
          {
            "uri": "http://nginx/",
            "param": "X-XSS-Protection",
            "method": "GET"
          },
          {
            "uri": "http://nginx/myform",
            "param": "X-XSS-Protection",
            "method": "POST"
          },
          {
            "uri": "http://nginx/robots.txt",
            "param": "X-XSS-Protection",
            "method": "GET"
          },
          {
            "uri": "http://nginx",
            "param": "X-XSS-Protection",
            "method": "GET"
          },
          {
            "uri": "http://nginx/sitemap.xml",
            "param": "X-XSS-Protection",
            "method": "GET"
          }
        ],
        "pluginid": "10016",
        "riskcode": "1",
        "wascid": "14",
        "solution": "<p>Ensure that the web browser's XSS filter is enabled, by setting the X-XSS-Protection HTTP response header to '1'.</p>",
        "cweid": "933",
        "desc": "<p>Web Browser XSS Protection is not enabled, or is disabled by the configuration of the 'X-XSS-Protection' HTTP response header on the web server</p>"
      },
      {
        "count": "2",
        "riskdesc": "Low (Medium)",
        "name": "X-Content-Type-Options Header Missing",
        "reference": "<p>http://msdn.microsoft.com/en-us/library/ie/gg622941%28v=vs.85%29.aspx</p><p>https://www.owasp.org/index.php/List_of_useful_HTTP_headers</p>",
        "otherinfo": "<p>This issue still applies to error type pages (401, 403, 500, etc) as those pages are often still affected by injection issues, in which case there is still concern for browsers sniffing pages away from their actual content type.</p><p>At \"High\" threshold this scanner will not alert on client or server error responses.</p>",
        "sourceid": "3",
        "confidence": "2",
        "alert": "X-Content-Type-Options Header Missing",
        "instances": [
          {
            "uri": "http://nginx/",
            "param": "X-Content-Type-Options",
            "method": "GET"
          },
          {
            "uri": "http://nginx",
            "param": "X-Content-Type-Options",
            "method": "GET"
          }
        ],
        "pluginid": "10021",
        "riskcode": "1",
        "wascid": "15",
        "solution": "<p>Ensure that the application/web server sets the Content-Type header appropriately, and that it sets the X-Content-Type-Options header to 'nosniff' for all web pages.</p><p>If possible, ensure that the end user uses a standards-compliant and modern web browser that does not perform MIME-sniffing at all, or that can be directed by the web application/web server to not perform MIME-sniffing.</p>",
        "cweid": "16",
        "desc": "<p>The Anti-MIME-Sniffing header X-Content-Type-Options was not set to 'nosniff'. This allows older versions of Internet Explorer and Chrome to perform MIME-sniffing on the response body, potentially causing the response body to be interpreted and displayed as a content type other than the declared content type. Current (early 2014) and legacy versions of Firefox will use the declared content type (if one is set), rather than performing MIME-sniffing.</p>"
      },
      {
        "count": "2",
        "riskdesc": "Medium (Medium)",
        "name": "X-Frame-Options Header Not Set",
        "reference": "<p>http://blogs.msdn.com/b/ieinternals/archive/2010/03/30/combating-clickjacking-with-x-frame-options.aspx</p>",
        "sourceid": "3",
        "confidence": "2",
        "alert": "X-Frame-Options Header Not Set",
        "instances": [
          {
            "uri": "http://nginx/",
            "param": "X-Frame-Options",
            "method": "GET"
          },
          {
            "uri": "http://nginx",
            "param": "X-Frame-Options",
            "method": "GET"
          }
        ],
        "pluginid": "10020",
        "riskcode": "2",
        "wascid": "15",
        "solution": "<p>Most modern Web browsers support the X-Frame-Options HTTP header. Ensure it's set on all web pages returned by your site (if you expect the page to be framed only by pages on your server (e.g. it's part of a FRAMESET) then you'll want to use SAMEORIGIN, otherwise if you never expect the page to be framed, you should use DENY. ALLOW-FROM allows specific websites to frame the web page in supported web browsers).</p>",
        "cweid": "16",
        "desc": "<p>X-Frame-Options header is not included in the HTTP response to protect against 'ClickJacking' attacks.</p>"
      },
      {
        "count": "2",
        "riskdesc": "Low (Medium)",
        "name": "Absence of Anti-CSRF Tokens",
        "reference": "<p>http://projects.webappsec.org/Cross-Site-Request-Forgery</p><p>http://cwe.mitre.org/data/definitions/352.html</p>",
        "otherinfo": "<p>No known Anti-CSRF token [anticsrf, CSRFToken, __RequestVerificationToken, csrfmiddlewaretoken, authenticity_token, OWASP_CSRFTOKEN, anoncsrf, csrf_token, _csrf, _csrfSecret] was found in the following HTML form: [Form 1: \"name\" ].</p>",
        "sourceid": "3",
        "confidence": "2",
        "alert": "Absence of Anti-CSRF Tokens",
        "instances": [
          {
            "evidence": "<form action=\"/myform\" method=\"POST\">",
            "uri": "http://nginx",
            "method": "GET"
          },
          {
            "evidence": "<form action=\"/myform\" method=\"POST\">",
            "uri": "http://nginx/",
            "method": "GET"
          }
        ],
        "pluginid": "10202",
        "riskcode": "1",
        "wascid": "9",
        "solution": "<p>Phase: Architecture and Design</p><p>Use a vetted library or framework that does not allow this weakness to occur or provides constructs that make this weakness easier to avoid.</p><p>For example, use anti-CSRF packages such as the OWASP CSRFGuard.</p><p></p><p>Phase: Implementation</p><p>Ensure that your application is free of cross-site scripting issues, because most CSRF defenses can be bypassed using attacker-controlled script.</p><p></p><p>Phase: Architecture and Design</p><p>Generate a unique nonce for each form, place the nonce into the form, and verify the nonce upon receipt of the form. Be sure that the nonce is not predictable (CWE-330).</p><p>Note that this can be bypassed using XSS.</p><p></p><p>Identify especially dangerous operations. When the user performs a dangerous operation, send a separate confirmation request to ensure that the user intended to perform that operation.</p><p>Note that this can be bypassed using XSS.</p><p></p><p>Use the ESAPI Session Management control.</p><p>This control includes a component for CSRF.</p><p></p><p>Do not use the GET method for any request that triggers a state change.</p><p></p><p>Phase: Implementation</p><p>Check the HTTP Referer header to see if the request originated from an expected page. This could break legitimate functionality, because users or proxies may have disabled sending the Referer for privacy reasons.</p>",
        "cweid": "352",
        "desc": "<p>No Anti-CSRF tokens were found in a HTML submission form.</p><p>A cross-site request forgery is an attack that involves forcing a victim to send an HTTP request to a target destination without their knowledge or intent in order to perform an action as the victim. The underlying cause is application functionality using predictable URL/form actions in a repeatable way. The nature of the attack is that CSRF exploits the trust that a web site has for a user. By contrast, cross-site scripting (XSS) exploits the trust that a user has for a web site. Like XSS, CSRF attacks are not necessarily cross-site, but they can be. Cross-site request forgery is also known as CSRF, XSRF, one-click attack, session riding, confused deputy, and sea surf.</p><p></p><p>CSRF attacks are effective in a number of situations, including:</p><p>    * The victim has an active session on the target site.</p><p>    * The victim is authenticated via HTTP auth on the target site.</p><p>    * The victim is on the same local network as the target site.</p><p></p><p>CSRF has primarily been used to perform an action against a target site using the victim's privileges, but recent techniques have been discovered to disclose information by gaining access to the response. The risk of information disclosure is dramatically increased when the target site is vulnerable to XSS, because XSS can be used as a platform for CSRF, allowing the attack to operate within the bounds of the same-origin policy.</p>"
      }
    ],
    "@ssl": "false"
  }
]
