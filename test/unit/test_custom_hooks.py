import os
import json
import unittest
from unittest.mock import MagicMock, call, mock_open, patch
from src.custom_hooks import CustomHooks
from dast_exception import DastException


class CustomHooksTest(unittest.TestCase):

    def setUp(self):
        self.hooks = CustomHooks()
        self.hooks.json_report = 'foo.json'
        self.zap = MagicMock()
        self.scan_data = {'id': 0, 'progress': '100', 'state': 'FINISHED'}
        self.full_result_data = [
                                    {'urlsInScope': []},
                                    {'urlsOutOfScope': []},
                                    {'urlsIoError': []}
                                ]
        self.zap.spider.scans = [self.scan_data]
        self.zap.spider.full_results = MagicMock(return_value=self.full_result_data)

    def test_cli_opts_with_report(self):
        acutal_name = 'foobar'
        opts = [('-J', acutal_name), ('-a', 'aaa'), ('-b', 'bbb')]
        self.hooks.cli_opts(opts)
        self.assertEqual(self.hooks.json_report, acutal_name)

    def test_cli_opts_without_report(self):
        opts = [('-a', 'aaa'), ('-b', 'bbb')]
        self.hooks.cli_opts(opts)
        self.assertIsNone(self.hooks.json_report)

    def test_zap_pre_shutdown_without_report(self):
        self.hooks.json_report = None
        self.hooks.zap_pre_shutdown(self.zap)  # should not raise

    def test_zap_pre_shutdown_no_scans(self):
        self.zap.spider.scans = []
        self.assertRaises(DastException, self.hooks.zap_pre_shutdown, self.zap)

    def test_zap_pre_shutdown_add_scanned_urls_to_report(self):
        with open('test/unit/fixtures/expected_report.json', 'r') as file:
            expected = json.load(file)

        with patch('builtins.open', new_callable=mock_open()) as fopen:
            with patch('json.load', return_value={'foo': 'bar'}) as jsonl:
                with patch('json.dump') as jsond:
                    self.hooks.zap_pre_shutdown(self.zap)
                    expected_path = os.path.join(self.hooks.WRK_DIR, self.hooks.json_report)
                    fopen.assert_called_with(expected_path, 'r+')
                    jsonl.assert_called_with(fopen().__enter__())
                    jsond.assert_called_with(expected, fopen().__enter__())


if __name__ == '__main__':
    unittest.main()
