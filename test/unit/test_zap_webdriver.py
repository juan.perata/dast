import unittest
from src.zap_webdriver import ZapWebdriver
from unittest.mock import MagicMock, call


class ZapWebdriverTest(unittest.TestCase):

    def setUp(self):
        self.webdriver = ZapWebdriver()
        self.webdriver.auth_username = 'user'
        self.webdriver.auth_password = 'pass'

    def test_auto_login_searches_and_fills_in_user(self):
        self.webdriver.driver = MagicMock()
        self.webdriver.auto_login(None, None)

        expectedCalls = [call("(//input[(@type='text' and contains(@name,'ser'))"
                              " or @type='text'])[1]"),
                         call().clear(),
                         call().send_keys(self.webdriver.auth_username)]
        self.webdriver.driver.find_element_by_xpath.assert_has_calls(expectedCalls)

    def test_auto_login_searches_and_fills_in_pass(self):
        self.webdriver.driver = MagicMock()
        self.webdriver.auto_login(None, None)

        expectedCalls = [call("//input[@type='password' or contains(@name,'ass')]"),
                         call().clear(),
                         call().send_keys(self.webdriver.auth_password)]
        self.webdriver.driver.find_element_by_xpath.assert_has_calls(expectedCalls)

    def test_auto_login_searches_and_clicks_submit(self):
        self.webdriver.driver = MagicMock()
        self.webdriver.auto_login(None, None)

        expectedCalls = [call("//*[(translate(@name, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ',"
                              "'abcdefghijklmnopqrstuvwxyz')='login' "
                              "and (@type='submit' or @type='button')) "
                              "or @type='submit' or @type='button']"),
                         call().click()]

        self.webdriver.driver.find_element_by_xpath.assert_has_calls(expectedCalls)


if __name__ == '__main__':
    unittest.main()
