#!/bin/sh

source ./test/test-webgoat-base.sh # $PWD is the project dir because this script is being run by test.sh

# Checking just for test report presence since ZAP test results may be slightly different on the same input and params
# and cause false positives when compared to a single expectation file.
if [ ! -f ./gl-dast-report.json ]; then
  echo "Expected a test report, but none was created"
  exit 1
fi

check_urls
