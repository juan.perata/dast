#!/bin/sh

export DAST_FULL_SCAN_ENABLED=true

source ./test/test-webgoat-base.sh # $PWD is the project dir because this script is being run by test.sh

# Install jq if absent
which jq > /dev/null || apk add jq

# "20012" is the plugin ID for "Anti CSRF Tokens Scanner" vulnerability rule which is the part of Beta quality Active Scan rules
# See https://github.com/zaproxy/zap-extensions/blob/3bee9c5430b5c698aa9a6bf0556608e0499303c8/addOns/ascanrulesBeta/src/main/java/org/zaproxy/zap/extension/ascanrulesBeta/Csrftokenscan.java#L55
TEST_PASS=$(jq '[.site[0].alerts[].pluginid == "20012"] | any' gl-dast-report.json)

if [ $TEST_PASS = "false" ]; then
  echo "Expected the report to contain \"Anti CSRF Tokens Scanner\" vulnerability occurrences but none found"
  exit 1
fi

check_urls
