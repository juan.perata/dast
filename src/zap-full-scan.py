#!/usr/bin/env python

# This script wraps zap-full-scan.py, which is coming with OWASP ZAP, for the purpose of supporting
# the syntax of GitLab DAST jobs as defined at
# https://docs.gitlab.com/ee/user/application_security/dast/index.html

import os
import argparse
import logging
import sys
import zap_full_scan_original
from scan_script_wrapper import ScanScriptWrapper


def main(argv):
    wrapper = ScanScriptWrapper(zap_full_scan_original)
    wrapper.run(argv)


if __name__ == "__main__":
    main(sys.argv[1:])
