#!/usr/bin/env python

import os
import argparse
import logging


class ScanScriptWrapper():
    # Supported auth params
    AUTH_PARAMS = [
        '--auth-first-page',  # left for backward compatibility only, could be removed in 12.0
                              # as a breaking change
        '--auth-url',
        '--auth-username',
        '--auth-password',
        '--auth-username-field',
        '--auth-password-field',
        '--auth-first-submit-field',
        '--auth-submit-field',
        '--auth-exclude-urls'
    ]

    def __init__(self, original_script_module):
        self.setup_logging()
        self.patch_original_usage(original_script_module)

    def setup_logging(self):
        logging.basicConfig(level=logging.INFO,
                            format='%(asctime)s %(message)s')
        # Hide "Starting new HTTP connection" messages
        logging.getLogger("requests").setLevel(logging.DEBUG)

    def patch_original_usage(self, original_script_module):
        self.original_usage = original_script_module.usage
        self.original_main = original_script_module.main
        original_script_module.usage = self.usage

    def usage(self):
        self.original_usage()
        print('')
        print('Authentication:')
        print('    --auth-url                 login form URL')
        print('    --auth-username            username')
        print('    --auth-password            password')
        print('    --auth-username-field      name of username input field')
        print('    --auth-password-field      name of password input field')
        print('    --auth-submit-field        name or value of submit input')
        print('    --auth-first-submit-field  name or value of submit input of first page')
        print('    --auth-exclude-urls        comma separated list of URLs to exclude '
              '(no spaces in between), supply all URLs causing logout')
        print('')
        print('For more details on authentication parameters see '
              'https://docs.gitlab.com/ee/user/application_security/dast/index.html')

    def run(self, argv):
        try:
            parser = argparse.ArgumentParser()
            for p in self.AUTH_PARAMS:
                parser.add_argument(p)

            args, unknown = parser.parse_known_args(argv)

            for opt, arg in vars(args).iteritems():
                if arg is not None:
                    os.environ[opt.upper()] = arg

            self.original_main(unknown)
        except Exception:
            raise
        else:
            pass
        finally:
            pass
