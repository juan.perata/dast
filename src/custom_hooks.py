import json
import logging
import os
from zap_webdriver import ZapWebdriver
from dast_exception import DastException


class CustomHooks():

    WRK_DIR = '/zap/wrk/'

    def __init__(self):
        self.opts = None
        self.json_report = None

    def zap_access_target(self, zap, target):
        logging.debug('zap_access_target')
        webdriver = ZapWebdriver()
        webdriver.load_from_environment_vars()

        try:
            if webdriver.is_authentication_required():
                webdriver.setup_zap_context(zap, target)
                webdriver.setup_webdriver(zap, target)
                webdriver.login(zap, target)
        except Exception as e:
            logging.error('Authentication Error')
            logging.exception(e)
        finally:
            webdriver.cleanup()

    def cli_opts(self, opts):
        logging.info('Script params: ' + str(opts))
        self.opts = opts

        json_report = [x[1] for x in self.opts if x[0] == '-J']
        self.json_report = json_report[0] if json_report else None

    def zap_pre_shutdown(self, zap):
        logging.debug('zap_pre_shutdown')

        if not self.json_report:  # no JSON report is generated
            return

        all_scans = zap.spider.scans
        if not all_scans:
            raise DastException('Error: no scans were performed. ' +
                                'Check log file zap.out to find out the reason.')

        # baseline scan and full scan should only perform one scan,
        # warn if for some reason multiple scans were performed
        if len(all_scans) > 1:
            logging.warning('More than one scan was executed. ' +
                            'Reporting only results from first run.')

        scan = all_scans[0]
        scan['result'] = {}
        full_result = zap.spider.full_results(int(scan['id']))

        # Remove data from full_results() that is
        # not needed in the test report.
        #
        # The removed fields are:
        # - scan.id
        # - full_results[].urlsInScope[].messageId
        #
        # For an explanation of the fields, see ZAP's docs
        # https://github.com/zaproxy/zaproxy/wiki/ApiGen_Index
        if 'id' in scan:
            del scan['id']
        for res in full_result:
            if 'urlsInScope' in res:
                for urls in res['urlsInScope']:
                    if 'messageId' in urls:
                        del urls['messageId']

            scan['result'].update(res)

        # add scanned URLs to the report file
        with open(os.path.join(self.WRK_DIR, self.json_report), 'r+') as file:
            report = json.load(file)
            file.seek(0)
            report.update({'spider': scan})
            json.dump(report, file)
            file.truncate()
