#!/bin/bash

mkdir -p /zap/wrk

# Get website URL from arguments
while getopts "t:" opt; do
  case $opt in
    t) DAST_WEBSITE="$OPTARG"
       shift 2
    break
    ;;
    \?) if [ -z "$DAST_WEBSITE" ]; then
        echo "Invalid option -$OPTARG" >&2
    fi
    ;;
  esac
done

if [ -z "$DAST_WEBSITE" ]; then
  echo "\$DAST_WEBSITE is empty, aborting. See https://docs.gitlab.com/ee/user/application_security/dast/ for configuration"
  exit 1
fi

# First, wait until the website is reachable
echo "Waiting for $DAST_WEBSITE to be available..."
if ! curl \
  --show-error \
  --silent \
  --fail  \
  --location \
  --retry 999 \
  --retry-connrefused \
  --max-time 10 \
  --retry-delay 5 \
  --retry-max-time ${DAST_TARGET_AVAILABILITY_TIMEOUT:-60} \
  $DAST_WEBSITE -o /dev/null; \
then
  echo "Warning, $DAST_WEBSITE could not be reached! Trying to scan anyway..."
fi

DAST_FULL_SCAN_ENABLED=${DAST_FULL_SCAN_ENABLED:-'false'}
DAST_FULL_SCAN_ENABLED_REGEX="^[Tt]rue|1$"
if [[ "$DAST_FULL_SCAN_ENABLED" =~ $DAST_FULL_SCAN_ENABLED_REGEX ]]; then
  echo "ZAP Full Scan started"
  /zap/zap-full-scan.py -J gl-dast-report.json -t $DAST_WEBSITE $@
else
  echo "ZAP Baseline Scan started"
  /zap/zap-baseline.py -J gl-dast-report.json -t $DAST_WEBSITE $@
fi

EXIT_CODE=$?

cp /zap/wrk/gl-dast-report.json .

# Cleanup background processes. Needed for Gitlab runner running on Kubernetes
pkill Xvfb 2>/dev/null

# Exit code 3 from zap-baseline means the tool failed. Translate that to 
# exit code 1
if [ $EXIT_CODE != 3 ]
then
  exit 0
else
  exit 1
fi
